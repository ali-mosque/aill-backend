<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Memorize_Student extends Model
{
    use HasFactory;

    protected $fillable = ['student_id', 'memorize_id', 'rate'];


    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function memorizes()
    {
        return $this->belongsTo(Memorizes::class);
    }
}
