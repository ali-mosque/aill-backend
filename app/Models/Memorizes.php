<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Memorizes extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function memorizes_students()
    {
        return $this->hasMany(Memorize_Student::class);
    }
}