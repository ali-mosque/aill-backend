<?php

namespace App\Enums;

abstract class StarsEffectEnum
{
    const levelUp = 1;
    const levelDown = 2;

}