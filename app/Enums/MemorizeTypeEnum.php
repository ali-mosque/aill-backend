<?php

namespace App\Enums;

abstract class MemorizeTypeEnum
{
    const checkCard = 5;
    const checkPart = 20;
    const advanceMemorize = 3;
    const chapterMemorize = 1;

}