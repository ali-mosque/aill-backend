<?php

namespace App\Http\Controllers\Save_Quran;

use App\Http\Controllers\Controller;
use App\Http\Requests\MemorizeRequest;
use Illuminate\Http\Request;
use App\Models\Memorizes;
use App\Models\Memorize_Student;
use App\Models\Student;
use App\Models\Level;
use App\Enums\MemorizeTypeEnum;
use App\Enums\StarsEffectEnum;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\DatabaseManager;



class MemorizeController extends Controller
{
 
    public function getMemorizeByStudentName(Request $request)
    {
        $searchTerm = $request->input('studentName');
        $students = DB::select("
        SELECT memorizes.*
FROM students
INNER JOIN memorize__students ON students.id = memorize__students.student_id
INNER JOIN memorizes ON memorize__students.memorize_id = memorizes.id
WHERE students.name LIKE '%$searchTerm%'
            
        ");
        return $students;
    }

    public function raceStatus(Request $request){
        
        $students = DB::select('
        WITH allStudent AS (
            SELECT students.id AS student_id, students.count_start, students.name, students.level_id, ROUND(SUM(m.count_point),2) as totalPoint
            FROM students
            LEFT JOIN memorize__students ms ON ms.student_id = students.id 
            LEFT JOIN memorizes m ON m.id = ms.memorize_id
            WHERE ms.created_at BETWEEN ? AND ?
            GROUP BY students.id, students.name, students.level_id, students.count_start
            ORDER BY students.level_id, totalPoint DESC
        )
        SELECT allStudent.*, level.name as level_name FROM allStudent
        left join levels level on allStudent.level_id = level.id 
    ', [$request->input('start_date'), $request->input('end_date')]);

            $groupedStudents = collect($students)->groupBy('level_name');
            return $groupedStudents;
    }


    public function store(Request $request)
    {
        $memorize = Memorizes::where('type', $request->type)
        ->when($request, function ($query, $request) {
            switch ($request->type) {
                case 'checkCard':
                    return $query->where('num_card',$request->card)->where('part',$request->part);
                case 'checkPart':
                    return $query->where('part',$request->part);
                case 'advanceMemorize':
                    return $query->where('num_page',$request->page)->where('part',$request->part);
                case 'chapterMemorize':
                    return $query->where('chapter',$request->chapter["name"]);
            }
        })
        ->first();   
        if( $memorize == null ) {
            $memorize = Memorizes::create([
                'num_page' => $request->page,
                'chapter' => $request->chapter["name"],
                'type' => $request->type,
                'num_card' => $request->card,
                'part' => $request->part,
                'count_point' => $this->getPoint($request),
            ]);
        }
        Memorize_Student::create([
            'student_id' => $request->student_id,
            'memorize_id' => $memorize->id,
            'rate' => $request->evaluation
        ]);
    }

    
    public function getPoint(Request $request)
    {
        if($request->type !== "chapterMemorize") 
           return constant(MemorizeTypeEnum::class . '::' . $request->type); 
        else 
            return $request->chapter["point"] ;
    }

    public function updateSate(Request $request)
    {
        $students = DB::select('
        SELECT students.id, students.count_start, students.name, students.level_id, students.id, SUM(m.count_point) as totalPoint
        FROM students
        LEFT JOIN memorize__students ms ON ms.student_id = students.id 
        LEFT JOIN memorizes m ON m.id = ms.memorize_id
        WHERE ms.created_at BETWEEN ? AND ?
        GROUP BY students.id, students.name, students.level_id, students.count_start
        ORDER BY students.level_id, totalPoint DESC
    ', [$request->input('start_date'), $request->input('end_date')]);

        $studentIds = collect($students)->pluck('id')->toArray();    
        foreach ($students as $student) {
            if (!$student->totalPoint) $student->totalPoint = 0;
            $student->new_level_id = $this->currentStudentLevelId($student);
        }
        $this->calculateStudentStars($students,$studentIds);
        $this->updateStudentsLevels($students,$studentIds);
        return $students;
    }


    public function updateStudentsLevels($students,$studentIds)
    {
        DB::transaction(function() use($studentIds, $students) {
            foreach ($students as $student) {                
                Student::where(['id' => $student->id])
                  ->update(['level_id' => $student->new_level_id]);
            }

        });
    }

    public function calculateStudentStars($students,$studentIds)
    {
        DB::transaction(function() use($studentIds, $students) {
            foreach ($students as $student) {                
                Student::where(['id' => $student->id])
                  ->update(['count_start' => $student->count_start + $this->getDeferentStarsNumber($student) + $this->currentStudentTopStars($students, $student) ]);
            }

        });
    }

    public function currentStudentTopStars ($students,$student){
        $groupedStudents = collect($students)->groupBy('level_id');
        $order = -1;

        foreach ($groupedStudents as $levelId => $students) {
            if(array_search($student, $students->take(3)->toArray()) !== false){
                $order= array_search($student, $students->take(3)->toArray());
                return $this->numberOfStarByOrder($order);
            }
        }
    }

    public function numberOfStarByOrder ($studentOrder){
        if ($studentOrder == 0)
            return 3;
        else if ($studentOrder == 1)
            return 2;
        else if ($studentOrder == 2)
            return 1;
        else
            return 0;
    }

    public function currentStudentLevelId($student) {
        switch ($student->totalPoint) {
            case ($student->totalPoint < 10):
                $student->new_level_id = 1;
                break;
            case ($student->totalPoint >= 10 && $student->totalPoint < 25):
                $student->new_level_id = 2;
                break;
            case ($student->totalPoint >= 25 && $student->totalPoint < 40):
                $student->new_level_id = 3;
                break;
            case ($student->totalPoint >= 40):
                $student->new_level_id = 4;
                break;
            default:
                break;
        }   
        return $student->new_level_id;
    }

    public function getDeferentStarsNumber($student)
    {
        $number_of_stars_moved =  $student->new_level_id - $student->level_id;
        if($number_of_stars_moved < 0) 
            $number_of_stars_moved = $number_of_stars_moved * StarsEffectEnum::levelDown;
        else
            $number_of_stars_moved = $number_of_stars_moved * StarsEffectEnum::levelUp;

        return $number_of_stars_moved;
    }

}
