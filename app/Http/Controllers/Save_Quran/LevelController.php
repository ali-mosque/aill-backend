<?php

namespace App\Http\Controllers\Save_Quran;

use App\Http\Controllers\Controller;
use App\Models\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{

    public function index()
    {
        $level = Level::where('id', ">", 0)->get();
        return response()->json($level, 200);
    }

    public function store(Request $request)
    {
        Level::create($request->all());
        return response()->json("Done Create Level", 201);


    }

    public function show($id)
    {
        $level = Level::where('id', $id)->first();
        return response()->json($level, 200);
    }


    public function update(Request $request, $id)
    {
        $level = Level::where('id', $id)->first();
        $level->update($request->all());

        return response()->json($level, 200);
    }


    public function destroy($id)
    {
        $level = Level::where('id', $id)->delete();
        if (!$level) {
            return response()->json('It does not exist actually', 200);
        }
        return response()->json('Done Delete Level', 200);

    }

}