<?php

namespace App\Http\Controllers\Save_Quran;

use App\Http\Controllers\Controller;
use App\Models\State;
use Illuminate\Http\Request;

class StateController extends Controller
{

    public function index()
    {
        $state = State::where('id', ">", 0)->get();
        return response()->json($state, 200);
    }


    public function store(Request $request)
    {
        State::create($request->all());
        return response()->json("Done Create state", 201);

    }


    public function show($id)
    {
        $state = State::where('id', $id)->first();
        return response()->json($state, 200);
    }


    public function update(Request $request, $id)
    {
        $state = State::where('id', $id)->first();
        $state->update($request->all());

        return response()->json($state, 200);
    }

    public function destroy($id)
    {
        $state = State::where('id', $id)->delete();
        if (!$state) {
            return response()->json('It does not exist actually', 200);
        }
        return response()->json('Done Delete state', 200);

    }
}