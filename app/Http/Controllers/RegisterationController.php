<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterationController extends Controller
{
    public function register(Request $request)
    {
        $request['password'] = bcrypt($request->password);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password
        ]);
        $token = $user->createToken("test")->plainTextToken;
        return response()->json([
            'user' => $user,
            'token' => $token
        ]);
    }

    public function login(Request $request)
    {
        if(!Auth::attempt($request->only(['email','password'])))
        {
            return response()->json(['Invalid email or password'],401);
        }
        $user=User::where('email',$request->email)->first();
        $token = $user->createToken($user->name)->plainTextToken;
        return response([
            'user' => $user,
            'token' =>$token
        ]);
    }

    public function logout(Request $request)
    {
        User::where('email',$request->email)->first()->tokens()->delete();

        return response([],200);
    }
}
