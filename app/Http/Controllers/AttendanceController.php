<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    public function store(Request $request)
    {
        $attendance = Attendance::create($request->all());
        return response()->json(['attendance' => $attendance], 200);
    }


    public function storeMany(Request $request)
    {
        $attendancees = $request->input('data');

        $test=[];
        foreach ($attendancees as $attendance) {
            try {
                $newAttendance = new Attendance();
                $newAttendance->student_id = $attendance['student_id'];
                $newAttendance->date = $attendance['date'];
                $newAttendance->day = $attendance['day'];
                $newAttendance->save();
                array_push($test,$newAttendance );
            } catch (\Exception $e) {
                continue;
            }
        }
        return $test;
        return response()->json(['message' => "saved "], 200);

    }
}
