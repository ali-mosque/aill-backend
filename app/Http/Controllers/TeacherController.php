<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function index()
    {
        $teachers = Teacher::orderBy('created_at','DESC')->get();
        return response()->json(['teachers' => $teachers],200);
    }

    public function create()
    {
        //
    }
    
    public function store(Request $request)
    {
       $teacher = Teacher::create($request->all());
       return response()->json(['teacher' => $teacher],200);
    }

    public function edit()
    {
        //
    }
    
    public function update(Request $request)
    {
        $teacher = Teacher::find($request->id)->update($request->all());
        return response()->json(['teacher' => $teacher],200);
    }

    public function destroy(Request $request)
    {
        Teacher::find($request->id)->delete();
        return response()->json([],200);
    }
}
