<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('state__students', function (Blueprint $table) {
            $table->id();
            $table->foreignId('state_id')->constrained()->onDelete('CASCADE')->default(null);
            $table->foreignId('student_id')->constrained()->onDelete('CASCADE')->default(null);
            $table->float('points');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('state__students');
    }
}