<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemorizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memorizes', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['checkPart', 'checkCard', 'advanceMemorize', 'chapterMemorize']);
            $table->integer('part')->nullable();
            $table->integer('num_page')->nullable();
            $table->integer('num_card')->nullable();
            $table->string('chapter')->nullable();
            $table->float('count_point');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memorizes');
    }
}