<?php

namespace Database\Seeders;

use App\Models\Classs;
use Illuminate\Database\Seeder;

class ClasssSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $class = [
            [
                "name" => "9"
            ]
        ];

        Classs::insert($class);

    }
}