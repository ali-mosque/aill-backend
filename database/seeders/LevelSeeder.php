<?php

namespace Database\Seeders;

use App\Models\Level;
use Illuminate\Database\Seeder;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = [
            [
                "id" => 1,
                "name" => "Level 4",
                "min_point" => 0
            ],
            [
                "id" => 2,
                "name" => "Level 3",
                "min_point" => 10
            ],
            [
                "id" => 3,
                "name" => "Level 2",
                "min_point" => 25
            ],
            [
                "id" => 4,
                "name" => "Level 1",
                "min_point" => 40
            ]
        ];

        Level::insert($level);
    }
}