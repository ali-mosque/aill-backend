<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'Jehad',
                'email' => 'jehad.shekhzain@gmail.com',
                'password' => Hash::make('12345678'),
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],

        ];

        User::insert($user);

    }
}