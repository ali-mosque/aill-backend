<?php

namespace Database\Seeders;

use App\Models\Memorizes;
use Illuminate\Database\Seeder;

class MemorizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $memorizes = [
            [
                "type" => "checkCard",
                "chapter" => null,
                "num_page" => null,
                "num_card" => 1,
                "part" => 1,
                "count_point" => 1,
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12'
            ]
        ];

        Memorizes::insert($memorizes);
    }
}