<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $student = [
            [
                "name" => "Kareem",
                "birth_date" => "1/1/1999",
                "phone" => "0934151507",
                "address" => "Maza",
                "father" => "samer",
                "classs_id" => "1",
                "level_id" => "1",
                "count_start" => 0
            ]
        ];

        Student::insert($student);
    }
}